package com.caf.cafbarcodescanner;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

public class ScannerActivity extends ActionBarActivity {

    private static final int SELECT_PHOTO = 18421;
    View mRootView;
    ImageView mBarcodeImage;
    TextView mBarcodeData;
    Button mImageSelect;
    private Button mScanImage;
    private Bitmap mSelectedImage;
    private BarcodeScanner mBarcodeScanner;
    private Button mFilterImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRootView = getLayoutInflater().inflate(R.layout.activity_scanner, null);
        setContentView(mRootView);
        extractObjectsFromIds();
        setListeners();
    }

    private void extractObjectsFromIds() {
        mBarcodeImage = (ImageView) mRootView.findViewById(R.id.barcode_image);
        mBarcodeData = (TextView) mRootView.findViewById(R.id.barcodedata);
        mImageSelect = (Button) mRootView.findViewById(R.id.choose_barcode_image);
        mScanImage = (Button) mRootView.findViewById(R.id.scan_barcode_image);
        mFilterImage = (Button) mRootView.findViewById(R.id.filter_barcode_image);
    }

    private void setListeners() {
        mImageSelect.setOnClickListener(createChooseImageListener());
        mScanImage.setOnClickListener(createScanImageListener());
        mFilterImage.setOnClickListener(createFilterImageListener());
    }

    private View.OnClickListener createFilterImageListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBarcodeScanner.applyFilter(new CAFScanCompletedListener() {
                    @Override
                    public void onScanComplete(CAFScanResult result) {
                        mBarcodeImage.setImageBitmap(result.binaryDisplayBitmap);
//                        mBarcodeData.setText(result.text);
                    }
                });
            }
        };
    }

    private View.OnClickListener createScanImageListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBarcodeScanner.scan(new CAFScanCompletedListener() {

                    @Override
                    public void onScanComplete(CAFScanResult result) {
                        mBarcodeImage.setImageBitmap(result.binaryDisplayBitmap);
                        String barcodeFormat = "";
                        if(result.rawResult != null)
                            barcodeFormat = result.rawResult.getBarcodeFormat().toString();
                        mBarcodeData.setText(barcodeFormat + " : " + result.text);
                    }
                });
            }
        };
    }

    private View.OnClickListener createChooseImageListener() {
        View.OnClickListener chooseImageListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        };
        return chooseImageListener;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scanner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SELECT_PHOTO: selectPhoto(data);
                                break;
        }
    }

    private void selectPhoto(Intent data) {
        try {

            Uri selectedImageUrl = data.getData();
            InputStream imageStream = getContentResolver().openInputStream(selectedImageUrl);
            mSelectedImage = BitmapFactory.decodeStream(imageStream);
            mBarcodeImage.setImageBitmap(mSelectedImage);
            mBarcodeScanner = new BarcodeScanner(mSelectedImage);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
