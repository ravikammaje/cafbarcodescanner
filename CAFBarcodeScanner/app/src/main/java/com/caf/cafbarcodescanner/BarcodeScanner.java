package com.caf.cafbarcodescanner;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.hardware.camera2.CameraManager;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ravi on 13/08/15.
 */
public class BarcodeScanner {
    Bitmap mOriginal;
    private CameraManager cameraManager;

    BarcodeScanner(Bitmap original) {
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, original.getWidth(), original.getHeight()), new RectF(0, 0, 1024, 1024), Matrix.ScaleToFit.CENTER);
        mOriginal = Bitmap.createBitmap(original, 0, 0, original.getWidth(), original.getHeight(), m, true);
    }

    public void scan(CAFScanCompletedListener scanCompletedListener) {

        CAFScanResult result = new CAFScanResult();
        result = scanBitmap(mOriginal);


        if (scanCompletedListener != null)
            scanCompletedListener.onScanComplete(result);
    }

    public void applyFilter(CAFScanCompletedListener scanCompletedListener) {
        CAFScanResult result = new CAFScanResult();
        result.binaryDisplayBitmap = filterBitmap(mOriginal);
        if (scanCompletedListener != null)
            scanCompletedListener.onScanComplete(result);
    }

    private Bitmap filterBitmap(Bitmap bmp) {
        byte[] bytes = getBitmapBytes(bmp);
        int[][]matrix = {
                {1,1,1,1,1},
                {1,1,1,1,1},
                {1,1,1,1,1}
        };
        byte[] newBytes = applyMatrix(bytes, bmp.getWidth(), bmp.getHeight(), matrix);
        Bitmap bitmap = getBitmap(bmp.getWidth(), bmp.getHeight(), newBytes);
        return bitmap;
    }

    private byte[] applyMatrix(byte[] bytes, int width, int height, int[][]filter) {
        int filterWidth = filter.length;
        int filterHeight = filter.length;
        byte[] newBytes = new byte[bytes.length];

        //apply the filter
        for(int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                double red = 0.0, green = 0.0, blue = 0.0;

                //multiply every value of the filter with corresponding image pixel
                for (int filterX = 0; filterX < filterWidth; filterX++)
                    for (int filterY = 0; filterY < filterHeight; filterY++) {
                        int imageX = (x - filterWidth / 2 + filterX);
                        int imageY = (y - filterHeight / 2 + filterY);
                        if (imageX >= 0 && imageY >= 0 && imageX < width && imageY < height) {
                            if ((imageX + 4 * width * imageY) > bytes.length) {
                                Log.e("BarcodeScanner", "index " + (imageX + 4 * width * imageY) + " is greater than byte array length:" + bytes.length);
                            }

                            red += bytes[0 + 4 * imageX + 4 * width * imageY] * filter[filterX][filterY];
                            green += bytes[1 + 4 * imageX + 4 * width * imageY] * filter[filterX][filterY];
                            blue += bytes[2 + 4 * imageX + 4 * width * imageY] * filter[filterX][filterY];
                        }
                    }
                double filterSize = (double) (filterWidth * filterHeight);
                red = red / filterSize;
                green = green / filterSize;
                blue = blue / filterSize;
                //truncate values smaller than zero and larger than 255
//                bytes[0+4*x + 4*width*y] = (byte)red;//(byte) Math.min(Math.max(red, 0), 255);
//                bytes[1+4*x + 4*width*y] = (byte)green;//(byte) Math.min(Math.max(green, 0), 255);
//                bytes[2+4*x + 4*width*y] = (byte)blue;//(byte) Math.min(Math.max(blue, 0), 255);
                newBytes[0 + 4 * x + 4 * width * y] = (byte) Math.min(Math.max(red, 0), 255);
                newBytes[1 + 4 * x + 4 * width * y] = (byte) Math.min(Math.max(green, 0), 255);
                newBytes[2 + 4 * x + 4 * width * y] = (byte) Math.min(Math.max(blue, 0), 255);

            }
        }
        return newBytes;

    }

    public byte[] getBitmapBytes(Bitmap bmp) {
        ByteBuffer buffer = ByteBuffer.allocate(bmp.getWidth()*bmp.getHeight()*4);
        bmp.copyPixelsToBuffer(buffer);
        return buffer.array();
    }

    private CAFScanResult scanBitmap(Bitmap bmp) {
        CAFScanResult result = new CAFScanResult();
        PlanarYUVLuminanceSource source = extractYUVLuminanceSource(bmp);
        if (source != null) {
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            result.binaryBitmap = bitmap;
            MultiFormatReader multiFormatReader = new MultiFormatReader();
            Map<DecodeHintType, ?> hints = getHints();
            multiFormatReader.setHints(hints);
            try {
                Result rawResult = multiFormatReader.decodeWithState(bitmap);
                result.rawResult = rawResult;
                result.text = rawResult.getText();
            } catch (NotFoundException e) {
                e.printStackTrace();
            }
            result.binaryDisplayBitmap = convertToDisplayBitmap(bitmap);
        }
        return result;
    }

    private Map<DecodeHintType, ?> getHints() {
        EnumMap<DecodeHintType, Object> hints = new EnumMap<>(DecodeHintType.class);

        Collection<BarcodeFormat> decodeFormats = EnumSet.noneOf(BarcodeFormat.class);
//        decodeFormats.addAll(DecodeFormatManager.PRODUCT_FORMATS);
//        decodeFormats.addAll(DecodeFormatManager.INDUSTRIAL_FORMATS);
//        decodeFormats.addAll(DecodeFormatManager.QR_CODE_FORMATS);
//        decodeFormats.addAll(DecodeFormatManager.DATA_MATRIX_FORMATS);
//        decodeFormats.addAll(DecodeFormatManager.AZTEC_FORMATS);
//        decodeFormats.addAll(DecodeFormatManager.PDF417_FORMATS);
        decodeFormats.add(BarcodeFormat.CODE_39);

        hints.put(DecodeHintType.POSSIBLE_FORMATS, decodeFormats);
        hints.put(DecodeHintType.TRY_HARDER, decodeFormats);
        hints.put(DecodeHintType.TRY_ALL_ANGLES, decodeFormats);

//        hints.put(DecodeHintType.CHARACTER_SET, characterSet);
//        hints.put(DecodeHintType.NEED_RESULT_POINT_CALLBACK, resultPointCallback);
//        Log.i("DecodeThread", "Hints: " + hints);
        return hints;
    }

    private Bitmap convertToDisplayBitmap(BinaryBitmap bitmap) {

        byte[] data = new byte[bitmap.getHeight()*bitmap.getWidth()*4];
        for(int i=0; i < bitmap.getHeight()*bitmap.getWidth(); i++) {
            int x = i % bitmap.getWidth();
            int y = i / bitmap.getWidth();
            try {
                data[4*i] = (byte)(bitmap.getBlackMatrix().get(x,y) ? 0 : 255);
                data[4*i+1] = (byte)(bitmap.getBlackMatrix().get(x,y) ? 0 : 255);
                data[4*i+2] = (byte)(bitmap.getBlackMatrix().get(x,y) ? 0 : 255);
                data[4*i+3] = (byte)(bitmap.getBlackMatrix().get(x,y) ? 255 : 255);
            } catch (NotFoundException e) {
                e.printStackTrace();
            }
        }
        return getBitmap(bitmap.getWidth(), bitmap.getHeight(), data);
    }

    @NonNull
    private Bitmap getBitmap(int width, int height, byte[] data) {
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        byteBuffer.rewind();
        bmp.copyPixelsFromBuffer(byteBuffer);
        return bmp;
    }

    private PlanarYUVLuminanceSource extractYUVLuminanceSource(Bitmap bmp) {
        Bitmap.Config config = bmp.getConfig();

        int[] pixels = new int[bmp.getWidth()*bmp.getHeight()];
        bmp.getPixels(pixels, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());
        byte[] data = new byte[(int) (bmp.getWidth()*bmp.getHeight()*2)];
        encodeYUV420SP(data, pixels, bmp.getWidth(), bmp.getHeight());


        return new PlanarYUVLuminanceSource(data, bmp.getWidth(),
                bmp.getHeight(), 0, 0, bmp.getWidth(),
                bmp.getHeight(), false);

    }

    void encodeYUV420SP(byte[] yuv420sp, int[] argb, int width, int height) {
        final int frameSize = width * height;

        int yIndex = 0;
        int uvIndex = frameSize;

        int a, R, G, B, Y, U, V;
        int index = 0;
        for (int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {

                a = (argb[index] & 0xff000000) >> 24; // a is not used obviously
                R = (argb[index] & 0xff0000) >> 16;
                G = (argb[index] & 0xff00) >> 8;
                B = (argb[index] & 0xff) >> 0;

                // well known RGB to YUV algorithm
                Y = ((66 * R + 129 * G + 25 * B + 128) >> 8) + 16;
                U = ((-38 * R - 74 * G + 112 * B + 128) >> 8) + 128;
                V = ((112 * R - 94 * G - 18 * B + 128) >> 8) + 128;

                // NV21 has a plane of Y and interleaved planes of VU each sampled by a factor of 2
                //    meaning for every 4 Y pixels there are 1 V and 1 U.  Note the sampling is every other
                //    pixel AND every other scanline.
                yuv420sp[yIndex++] = (byte) ((Y < 0) ? 0 : ((Y > 255) ? 255 : Y));
                if (j % 2 == 0 && index % 2 == 0) {
                    yuv420sp[uvIndex++] = (byte) ((V < 0) ? 0 : ((V > 255) ? 255 : V));
                    yuv420sp[uvIndex++] = (byte) ((U < 0) ? 0 : ((U > 255) ? 255 : U));
                }

                index++;
            }
        }
    }
}
