package com.caf.cafbarcodescanner;

/**
 * Created by ravi on 13/08/15.
 */
public interface CAFScanCompletedListener {

    public void onScanComplete(CAFScanResult result);
}
