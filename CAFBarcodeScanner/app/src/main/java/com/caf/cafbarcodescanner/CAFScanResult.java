package com.caf.cafbarcodescanner;

import android.graphics.Bitmap;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.Result;

/**
 * Created by ravi on 13/08/15.
 */
public class CAFScanResult {
    public String text;
    public BinaryBitmap binaryBitmap;
    public Result rawResult;
    public Bitmap binaryDisplayBitmap;
}
